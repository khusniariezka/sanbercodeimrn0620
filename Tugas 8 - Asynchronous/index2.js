var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000}
]

var a = 10000

readBooksPromise (a, books[0]) 
.then(function (b) {return readBooksPromise (b, books[1])})
.then(function (c) {return readBooksPromise (c, books[2])})
.catch(function (error) {return error.message});