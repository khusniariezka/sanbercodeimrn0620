// Soal 1
console.log("---SOAL 1---")
function range(startNum, finishNum) {
	var a = [], b = startNum;
    if (b < finishNum){
		while (b < finishNum+1) {
			a.push(b++);
		}
	} else if (b > finishNum) {
		while (b > finishNum-1) {
			a.push(b--);
		}
	} else {
		a.push(-1);
	}
    return a;
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

// Soal 2
console.log("---SOAL 2---")
function rangeWithStep(startNum, finishNum, step) {
    var a = [startNum], b = startNum;
    if (b < finishNum){
		while (b < finishNum) {
			a.push(b += step);
		}
		return (b > finishNum) ? a.slice(0,-1) : a;
	} else if (b > finishNum) {
		while (b > finishNum) {
			a.push(b -= step);
		}
		return (b < finishNum) ? a.slice(0,-1) : a;
	}
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
console.log("---SOAL 3---")
function sum(startNum, finishNum, step) {
	var a = 0;
	var array;
	
	if (!startNum && !finishNum && !step) {
		a = 0;
	} else if (startNum && !finishNum && !step) {
		a = startNum;
	} else {
		if (!step) {
			array = rangeWithStep(startNum, finishNum, 1)
		} else {
			array = rangeWithStep(startNum, finishNum, step)
		}
		for (var i = 0; i < array.length; i++) {
			a = a + parseInt(array[i])
		}
	}
	return a;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal 4
console.log("---SOAL 4---")
function dataHandling() {
	var d = "";
	var input = [
		["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
		["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
		["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
		["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
	]
	for (var i = 0; i < input.length; i++) {
		console.log("Nomor ID:  "+input[i][0])
		console.log("Nama Lengkap:  "+input[i][1])
		console.log("TTL:  "+input[i][2]+" "+input[i][3])
		console.log("Hobi:  "+input[i][4])
		console.log(" ")
	}
	return d;
}
console.log(dataHandling())

// Soal 5
console.log("---SOAL 5---")
function balikKata(a) {
	var d = "";
	var str = a; 
	for (var i = str.length - 1; i >= 0; i--) {
		process.stdout.write(str[i]);
	}
	return d;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
console.log("---SOAL 6---")
function dataHandling2() {
	var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
	var d = ""
	
	var name = input[1].split(" ");
	name.push("Elsharawy");
	var joinName = name.join(" ");

	var city = input[2].split(" ");
	city.splice(0, 0, "Provinsi");
	var joinCity = city.join(" ");

	input.splice(1, 2, joinName, joinCity);
	input.pop();
	input.push("Pria", "SMA Internasional Metro");
	console.log(input); // print full

	var date = input[3].split("/");
	var bulan = parseInt(date[1]);
	switch (bulan) {
	  case 1: 
		bulan = "Januari";
		break;
	  case 2: 
		bulan = "Februari";
		break;
	  case 3: 
		bulan = "Maret";
		break;
	  case 4: 
		bulan = "April";
		break;
	  case 5: 
		bulan = "Mei";
		break;
	  case 6: 
		bulan = "Juni";
		break;
	  case 7: 
		bulan = "Juli";
		break;
	  case 8: 
		bulan = "Agustus";
		break;
	  case 9: 
		bulan = "September";
		break;
	  case 10: 
		bulan = "Oktober";
		break;
	  case 11: 
		bulan = "November";
		break;
	  case 12: 
		bulan = "Desember";
		break;
	  default:
		break;
	}
	console.log(bulan);  // print month
	console.log([date[2],date[0],date[1]]);  // print date

	var joinDate = date.join("-");
	console.log(joinDate);  // print date 2

	var midName = input[1].slice(0,15);
	console.log(midName);  // print 15 char name
	return d;
}
console.log(dataHandling2());
