import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Nav from './Tugas/Tugas15/index';
import MyApp from './Latihan/MyApp/index';
import Quiz from './Tugas/Quiz3/index';
import LatAPI from './Latihan/LatihanAPI/index';
import MyProject from './Project/index';
import MyProduct from './Project/ProductScreen';

export default function App() {
  return (
    <MyProject />
  );
}