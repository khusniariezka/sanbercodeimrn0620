import React from "react";
import { LinearGradient } from "expo-linear-gradient";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Opening1 = ({navigation}) => {
  return (
    <ScreenContainer>
      <View style={styles.img}>
        <Image source={require('./images/1.png')} style={{width:300, height:300}}/>
      </View>
      <View style={styles.textOpen}>
        <Text style={styles.title}>SELAMAT DATANG</Text>
        <Text style={styles.content}>Contrary to popular belief, Lorem bla Ipsum is not simply random text. It literally has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock</Text>
        <View style={styles.circle}>
          <View style={styles.circleActive}></View>
          <View style={styles.circleChild}></View>
          <View style={styles.circleChild}></View>
        </View>
      </View>
      <View style={styles.btnSet}>
      <TouchableOpacity onPress={() => navigation.navigate('Login')} >
          <LinearGradient style={styles.btnSkip} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>S K I P</Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Open2')} >
          <LinearGradient style={styles.btnNext} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>N E X T</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </ScreenContainer>
  );
};

export const Opening2 = ({navigation}) => (
  <ScreenContainer>
    <View style={styles.img}>
        <Image source={require('./images/2.png')} style={{width:300, height:300}}/>
      </View>
      <View style={styles.textOpen}>
        <Text style={styles.title}>BAYAR DIMANA SAJA</Text>
        <Text style={styles.content}>Contrary to popular belief, Lorem bla Ipsum is not simply random text. It literally has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock</Text>
        <View style={styles.circle}>
          <View style={styles.circleChild}></View>
          <View style={styles.circleActive}></View>
          <View style={styles.circleChild}></View>
        </View>
      </View>
      <View style={styles.btnSet}>
      <TouchableOpacity onPress={() => navigation.navigate('Login')} >
          <LinearGradient style={styles.btnSkip} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>S K I P</Text>
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Open3')} >
          <LinearGradient style={styles.btnNext} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>N E X T</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
  </ScreenContainer>
);

export const Opening3 = ({navigation}) => (
  <ScreenContainer>
    <View style={styles.img}>
        <Image source={require('./images/3.png')} style={{width:300, height:300}}/>
      </View>
      <View style={styles.textOpen}>
        <Text style={styles.title}>BARANG DIJAMIN ASLI</Text>
        <Text style={styles.content}>Contrary to popular belief, Lorem bla Ipsum is not simply random text. It literally has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock</Text>
        <View style={styles.circle}>
          <View style={styles.circleChild}></View>
          <View style={styles.circleChild}></View>
          <View style={styles.circleActive}></View>
        </View>
      </View>
      <View style={styles.btnStr}>
      <TouchableOpacity onPress={() => navigation.navigate('Login')} >
          <LinearGradient style={styles.btnStart} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>GET STARTED</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
  </ScreenContainer>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 100,
    marginBottom: 10,
  },
  textOpen: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    marginHorizontal: 30,
  },
  title: {
    fontSize: 30,
    marginVertical: 10,
    color: '#4793D7',
  },
  content: {
    textAlign: 'center',
    lineHeight: 25,
    fontSize: 16,
  },
  circle: {
    flexDirection: 'row',
    marginVertical: 50,
  },
  circleChild: {
    backgroundColor: '#c4c4c4',
    borderRadius: 50,
    width: 8,
    height: 8,
    marginHorizontal: 8,
  },
  circleActive: {
    backgroundColor: '#4793D7',
    borderRadius: 50,
    width: 8,
    height: 8,
    marginHorizontal: 8,
  },
  btnSet: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnNext: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    height: 45,
    paddingVertical: 5,
    paddingLeft: 10,
    borderTopLeftRadius: 30,
    borderBottomLeftRadius: 30,
    elevation: 3,
  },
  btnSkip: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    height: 45,
    paddingVertical: 5,
    paddingRight: 10,
    borderTopRightRadius: 30,
    borderBottomRightRadius: 30,
    elevation: 3,
  },
  btnStr: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnStart: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 250,
    height: 45,
    padding: 5,
    borderRadius: 30,
    elevation: 3,
  },
  textButton: {
    color: 'white',
    fontSize: 20,
  },
});