import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from "expo-linear-gradient";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Detail1 = ({navigation}) => {
  return (
    <ScreenContainer>
      <StatusBar translucent={false} backgroundColor={'white'}/>
      <View style={styles.header}>
          <TouchableOpacity><Icon name="chevron-left" size={25} onPress={() => navigation.goBack()} /></TouchableOpacity>
          <Text>Detail Produk</Text>
          <TouchableOpacity><Icon name="menu" size={25} onPress={() => navigation.toggleDrawer()} /></TouchableOpacity>
      </View>
      <View style={styles.cardImage}>
        <Image style={styles.image} source={{ uri: `https://cdn.shopify.com/s/files/1/1338/0845/collections/lippie-pencil_grande.jpg?v=1512588769` }} />
        <TouchableOpacity style={styles.like}><Icon style={{color: '#ff160a'}} name="favorite" size={35} /></TouchableOpacity>
        <TouchableOpacity style={styles.bookmark}><Icon name="bookmark-border" size={35} /></TouchableOpacity>
      </View>
      <View style={styles.cardText}>
        <Text style={styles.title}>Lippie Pencil - Long wearing and high intensity lip pencil that glides easily</Text>
        <View style={styles.discText}>
            <Text style={styles.disc}>Rp 150.000 </Text>
            <Text> 20% OFF</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.price}>Rp 100.000</Text>  
          <View style={styles.rate}>
            <Icon style={{color: '#fc9d03'}} name="star" size={40} />
            <Text style={styles.rateText}>4,5</Text>
          </View>
        </View>     
      </View>

      <View style={styles.line}/>

      <View style={styles.cardText}>
        <Text style={styles.titleDesc}>RINCIAN PRODUK</Text>
        <Text>Lippie Pencil A long-wearing and high-intensity lip pencil that glides on easily and prevents feathering. Lippie Stix have a coordinating Lippie Pencil designed to compliment because no it perfectly. Many of our Lippie Stix have a coordinating Lippie Pencil designed to compliment it perfectly, but feel free to mix and match!</Text>
      </View>
      <View style={styles.btnSet}>
        <TouchableOpacity>
          <LinearGradient style={styles.btnChat} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Icon style={{color: '#e5e5e5'}} name="chat" size={30} />
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity>
          <LinearGradient style={styles.btnChat} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Icon style={{color: '#e5e5e5'}} name="shopping-cart" size={30} />
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity>
          <LinearGradient style={styles.btnBuy} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Text style={styles.buy}>Beli Sekarang</Text><Icon style={{color: '#e5e5e5'}} name="shopping-basket" size={30} />
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </ScreenContainer>
  );
};

export const Detail2 = ({navigation}) => {
  return (
    <ScreenContainer>
      <StatusBar translucent={false} backgroundColor={'white'}/>
      <View style={styles.header}>
          <TouchableOpacity><Icon name="chevron-left" size={25} onPress={() => navigation.goBack()} /></TouchableOpacity>
          <Text>Detail Produk</Text>
          <TouchableOpacity><Icon name="menu" size={25} onPress={() => navigation.toggleDrawer()} /></TouchableOpacity>
      </View>
      <View style={styles.cardImage}>
        <Image style={styles.image} source={{ uri: `https://static-assets.glossier.com/production/spree/images/attachments/000/001/476/portrait_normal/carousel-1.jpg?1501534121` }} />
        <TouchableOpacity style={styles.like}><Icon style={{color: '#ff160a'}} name="favorite" size={35} /></TouchableOpacity>
        <TouchableOpacity style={styles.bookmark}><Icon name="bookmark-border" size={35} /></TouchableOpacity>
      </View>
      <View style={styles.cardText}>
        <Text style={styles.title}>Wowder - It’s not powder, it’s matte not flat finish</Text>
        <View style={styles.discText}>
            <Text style={styles.disc}>Rp 560.000 </Text>
            <Text> 10% OFF</Text>
        </View>
        <View style={styles.info}>
          <Text style={styles.price}>Rp 420.000</Text>  
          <View style={styles.rate}>
            <Icon style={{color: '#fc9d03'}} name="star" size={40} />
            <Text style={styles.rateText}>4,5</Text>
          </View>
        </View>     
      </View>

      <View style={styles.line}/>

      <View style={styles.cardText}>
        <Text style={styles.titleDesc}>RINCIAN PRODUK</Text>
        <Text>It’s not powder, it’s Wowder—for non-dewy days. Wowder will: cut shine, blur the appearance of pores, set makeup, look like skin. Wowder won’t: leave a chalky film, cake into fine lines, flatten skin’s texture, or announce its presence in photographs (no camera flashback). In three sheer, glowy, adaptable shades...because no skin tone is HD-white.</Text>
      </View>
      <View style={styles.btnSet}>
        <TouchableOpacity>
          <LinearGradient style={styles.btnChat} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Icon style={{color: '#e5e5e5'}} name="chat" size={30} />
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity>
          <LinearGradient style={styles.btnChat} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Icon style={{color: '#e5e5e5'}} name="shopping-cart" size={30} />
          </LinearGradient>
        </TouchableOpacity>
        <TouchableOpacity>
          <LinearGradient style={styles.btnBuy} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Text style={styles.buy}>Beli Sekarang</Text><Icon style={{color: '#e5e5e5'}} name="shopping-basket" size={30} />
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </ScreenContainer>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
    },
    cardImage: {
      alignItems: 'center',
      position: 'relative',
    },
    image: {
      height: 300,
      width: 400,
    },
    like: {
      position: 'absolute',
      bottom: 20,
      right: 70,
    },
    bookmark: {
      position: 'absolute',
      bottom: 20,
      right: 20,
    },
    cardText: {
      padding: 20,
    },
    title: {
      fontSize: 20,
    },
    discText: { 
      flexDirection: 'row',
    },
    disc: {
        textDecorationLine: 'line-through', 
        textDecorationStyle: 'solid',
        color: '#c4c4c4',
    },
    price: {
        fontSize: 30,
        color: '#4793D7',
    },
    info: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    rate: {
      flexDirection: 'row',
    },
    rateText: {
        fontSize: 30,
    },
    line: {
        width: 500,
        height: 1,
        backgroundColor: '#c4c4c4',
    },
    titleDesc: {
        fontSize: 20,
        color: '#4793D7',
    },
    btnSet: {
      justifyContent: 'space-between',
      flexDirection: 'row',
      paddingTop: 20,
    },
    btnChat: {
      width: 80,
      height: 45, 
      marginLeft: 20,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 3,
      borderRadius: 50,
  },
  btnCart: {
    width: 80,
    height: 45, 
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
    borderRadius: 50,
},
  btnBuy: {
      width: 190,
      height: 45, 
      marginLeft: 12,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      elevation: 3,
      borderTopLeftRadius: 30,
      borderBottomLeftRadius: 30,
  },
  buy: {
    color: 'white',
    fontSize: 18,
    padding: 10,
  },
  textButton: {
      color: 'white',
  },
  }); 