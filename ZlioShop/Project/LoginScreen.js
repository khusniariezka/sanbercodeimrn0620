import React, { Component } from 'react';
import { LinearGradient } from "expo-linear-gradient";
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Reg = ({navigation}) => {
  return (
    <ScreenContainer>
      <View style={styles.topLogo}>
        <Image source={require('./images/logo1.png')} style={{width:150, height:250}} />
      </View>
      <View style={styles.formLogin}>
        <TextInput style={styles.inputSet} placeholder="Username" />
        <TextInput secureTextEntry={true} style={styles.inputSet} placeholder="Password" />
        <TextInput secureTextEntry={true} style={styles.inputSet} placeholder="Re-type password" />
        <TouchableOpacity onPress={() => navigation.navigate('Login')} >
        <LinearGradient style={styles.btnLogin} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
          <Text style={styles.textButton}>R E G I S T E R</Text>
          </LinearGradient>
        </TouchableOpacity>
        <Text style={styles.note}>
            <Text>Have an account </Text>
            <Text style={styles.link} onPress={() => navigation.navigate('Login')} >Sign In</Text>
            <Text> here</Text>
        </Text>
      </View>
    </ScreenContainer>
  );
};

export const Login = ({navigation}) => {
    return (
      <ScreenContainer>
        <View style={styles.topLogo}>
          <Image source={require('./images/logo1.png')} style={{width:150, height:250}} />
        </View>
        <View style={styles.formLogin}>
          <TextInput style={styles.inputSet} placeholder="Username" />
          <TextInput secureTextEntry={true} style={styles.inputSet} placeholder="Password" />
          <TouchableOpacity onPress={() => navigation.navigate('Main')} >
          <LinearGradient style={styles.btnLogin} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
            <Text style={styles.textButton}>L O G I N</Text>
            </LinearGradient>
          </TouchableOpacity>
          <Text style={styles.note}>
              <Text>Have an account </Text>
              <Text style={styles.link} onPress={() => navigation.navigate('Reg')} >Sign Up</Text>
              <Text> here</Text>
          </Text>
        </View>
      </ScreenContainer>
    );
  };
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    topLogo: {
        marginBottom: 40,
    },
    formLogin: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputSet: {
        width: 300, 
        height: 45, 
        margin: 12,
        borderColor: '#4793D7', 
        backgroundColor: 'white',
        elevation: 2,
        borderWidth: 1,
        borderRadius: 50,
        paddingHorizontal: 20,
    },
    btnLogin: {
        width: 300, 
        height: 45, 
        margin: 12,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
        borderRadius: 50,
    },
    textButton: {
        color: 'white',
    },
    note: {
        flexDirection: 'row',
    },
    link: {
        color: '#4793D7',
        fontWeight: 'bold',
    },
  }); 