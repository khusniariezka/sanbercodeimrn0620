import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from "expo-linear-gradient";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator, ScrollView, TouchableOpacity } from 'react-native';
import Axios from 'axios';

export default class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`http://makeup-api.herokuapp.com/api/v1/products.json`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <View>
        <StatusBar translucent={false} backgroundColor={'white'}/>
        <View style={styles.header}>
          <TouchableOpacity><Icon name="chevron-left" size={25} onPress={() => {this.props.navigation.goBack()}} /></TouchableOpacity>
          <Text>Produk Kecantikan</Text>
          <TouchableOpacity><Icon name="menu" size={25} onPress={() => navigation.toggleDrawer()} /></TouchableOpacity>
        </View>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal>
          <View style={styles.btnSet}>
            <LinearGradient style={styles.btnChildActive} colors={['#4793D7', '#9B5ED7']} start={[0, 0]} end={[1, 0]} >
              <Text style={styles.textButtonActive}>Kecantikan</Text>
            </LinearGradient>
            <View style={styles.btnChild} >
              <Text style={styles.textButton}>Perlengkapan Rumah</Text>
            </View>
            <View style={styles.btnChild} >
              <Text style={styles.textButton}>Fashion Terbaru</Text>
            </View>
            <View style={styles.btnChild} >
              <Text style={styles.textButton}>Makanan dan Minuman</Text>
            </View>
            <View style={styles.btnChild} >
              <Text style={styles.textButton}>Gadget dan Elektronik</Text>
            </View>
          </View>
        </ScrollView>
        
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <View style={styles.card}>
                <Image source={{ uri: `${item.image_link}` }} style={styles.cardImage} />
                <Text style={styles.title} numberOfLines={1}>{item.name}</Text>
                <Text style={styles.desc} numberOfLines={2}>{item.description}</Text>
                <Text style={styles.price}>$ {item.price}</Text>
            </View>
          }
          keyExtractor={({ id }, index) => index}
          horizontal={false}
          numColumns={2}
        />
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
},
headerStyle: {
    backgroundColor: '#74b0e8',
    paddingBottom: 15,
    elevation: 5,
  },
  btnSet: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  btnChild: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#4793D7',
    marginHorizontal: 5,
    padding: 10,
    borderWidth: 1,
    height: 30,
    borderRadius: 30,
  },
  btnChildActive: {
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    padding: 10,
    height: 30,
    borderRadius: 30,
    elevation: 2,
  },
  textButton: {
    color: '#4793D7',
  },
  textButtonActive: {
    color: 'white',
  },
  card: {
    backgroundColor: 'white', 
    width: 175,
    height: 280,
    borderRadius: 10,
    margin: 10,
    elevation: 5,
  },
  cardImage: {
      width: 175, 
      height: 170, 
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
  },
  title: {
      marginTop: 8, 
      marginHorizontal: 8, 
      fontSize: 16,
  },
  desc: {
      marginHorizontal: 8, 
      fontSize: 14,
      color: '#666',
  },
  price: {
      margin: 8, 
      fontSize: 20,
      color: '#4793D7',
  },
})