import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const About = ({navigation}) => {
    return (
      <ScreenContainer>
        <StatusBar translucent={false} backgroundColor={'white'}/>
        <View style={styles.header}>
            <TouchableOpacity><Icon name="chevron-left" size={25} onPress={() => navigation.navigate('Home')} /></TouchableOpacity>
            <TouchableOpacity><Icon name="menu" size={25} onPress={() => navigation.toggleDrawer()} /></TouchableOpacity>
        </View>
        <View style={styles.about}>
            <View style={styles.aboutMe}><Text style={styles.aboutTitle}>ABOUT ME</Text></View>
            <View style={styles.profile}>
                <Image style={styles.profileImage} source={require('./images/profile.png')} />
                <View style={styles.profileText}>
                    <Text style={styles.profileName}>Riezka Layli Khusnia</Text>
                    <Text style={styles.profileSkill}>Programmer & Web Designer</Text>
                </View>
            </View>
            <View style={styles.profileDesc}>
                <Text style={styles.descTitle}>DESCRIPTION</Text>
                <Text style={styles.desc}>Code is a lightweight but powerful source code editor which runs on your desktop and is available. It comes with built-in support for TypeScript powerful sourcecode editor on your desktop.</Text>
            </View>
        </View>
        <View style={styles.sosmedBox}>
            <View style={styles.line}/>
            <TouchableOpacity style={styles.cardSkillfb} >
                <Image style={styles.sosmed} source={require('./images/sm-fb.png')} />
                <Text>khusniariezka</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cardSkilltw} >
                <Image style={styles.sosmed} source={require('./images/sm-tw.png')} />
                <Text>@khusniariezka</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cardSkillig} >
                <Image style={styles.sosmed} source={require('./images/sm-ig.png')} />
                <Text>@khusniariezka</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.cardSkillgit} >
                <Image style={styles.sosmed} source={require('./images/gitlab.png')} />
                <Text>khusniariezka</Text>
            </TouchableOpacity>
        </View>
      </ScreenContainer>
    );
  };
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF7ED',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
    },
    about: {
        flex: 1,
    },
    aboutMe: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    aboutTitle: {
        fontSize: 30,
    },
    profile: {
        flexDirection: 'row',
        marginVertical: 10,
        marginHorizontal: 20,
    },
    profileImage: {
        height: 80,
        width: 80,
    },
    profileText: {
        margin: 15,
    },
    profileName: {
        fontSize: 20,
    },
    profileDesc: {
        marginVertical: 10,
        marginHorizontal: 20,
    },
    descTitle: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    desc: {
        lineHeight: 25,
        fontSize: 16,
        marginVertical: 10,
    },
    sosmedBox: {
        backgroundColor: 'white',
        height: 400,
        elevation: 5,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    line: {
        width: 25,
        height: 3,
        backgroundColor: '#c4c4c4',
        marginVertical: 20,
    },
    cardSkillfb: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        margin: 10,
        width: 300,
        borderRadius: 10,
        elevation: 3,
        backgroundColor:'#D0D8FF',
    },
    cardSkilltw: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        margin: 10,
        width: 300,
        borderRadius: 10,
        elevation: 3,
        backgroundColor:'#CBE1FF',
    },
    cardSkillig: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        margin: 10,
        width: 300,
        borderRadius: 10,
        elevation: 3,
        backgroundColor:'#FFC1C1',
    },
    cardSkillgit: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        margin: 10,
        width: 300,
        borderRadius: 10,
        elevation: 3,
        backgroundColor:'#FFDCB2',
    },
    sosmed: {
        width: 30,
        height: 30,
        marginHorizontal: 10,
    },
  }); 