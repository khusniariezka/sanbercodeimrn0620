import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Main = ({navigation}) => {
  return (
    <ScreenContainer>
        <StatusBar translucent={false} backgroundColor={'white'}/>
        <View style={styles.headerStyle}>
            <View style={styles.header}>
                <Image source={require('./images/logo2.png')} style={{width:60, height:20}} />
                <Icon style={styles.navItem} name="menu" size={25} onPress={() => navigation.toggleDrawer()} />
            </View>
            <View style={styles.searchBar}>
                <Icon style={styles.searchIcon} name="search" size={25} />
                <TextInput style={styles.search} placeholder="Search" />
            </View>
        </View>
        
        <ScrollView>
            <View style={styles.banner}>
                <Image style={styles.image} source={require('./images/banner.jpg')} />
            </View>
            <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                <View style={styles.menuSet}>
                    <View style={styles.menuRow}>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/CL.png')} />
                            <Text>Fashion</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/KD.png')} />
                            <Text>Kids</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/EL.png')} />
                            <Text>Watch</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/EC.png')} />
                            <Text>Electronic</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/BT.png')} />
                            <Text>Beauty</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/PT.png')} />
                            <Text>Eid Promo</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={styles.menuRow}> 
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/AC.png')} />
                            <Text>Aksesoris</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/MD.png')} />
                            <Text>Medicine</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/FB.png')} />
                            <Text>Food</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/DC.png')} />
                            <Text>Home</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/PE.png')} />
                            <Text>Pet</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.menuIcon}>
                            <Image style={styles.menuImage} source={require('./images/RM.png')} />
                            <Text>Bed</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.product}>
                <View style={styles.productText}>
                    <Text style={styles.text}>KOLEKSI PILIHAN</Text>
                    <TouchableOpacity style={styles.more}>
                        <Text style={styles.textMore}>Lihat lainnya</Text>
                        <Icon style={styles.text} name="chevron-right" size={20} />
                    </TouchableOpacity>
                </View>
                <View style={styles.cardSet}>
                    <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('Detail1')} >
                        <Image style={styles.cardImage} source={{ uri: `https://cdn.shopify.com/s/files/1/1338/0845/collections/lippie-pencil_grande.jpg?v=1512588769` }} />
                        <Text style={styles.cardText} numberOfLines={2}>Lippie Pencil - A long wearing and high intensity lip pencil that glides on easily and prevents feathering</Text>
                        <View style={styles.discText}>
                            <Text style={styles.disc}>Rp 150.000</Text>
                            <Text>20% OFF</Text>
                        </View>
                        <Text style={styles.price}>Rp 100.000</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('Detail2')} >
                        <Image style={styles.cardImage} source={{ uri: `https://static-assets.glossier.com/production/spree/images/attachments/000/001/476/portrait_normal/carousel-1.jpg?1501534121` }} />
                        <Text style={styles.cardText} numberOfLines={2}>Wowder - It’s not powder, it’s matte not flat finish</Text>
                        <View style={styles.discText}>
                            <Text style={styles.disc}>Rp 560.000</Text>
                            <Text>10% OFF</Text>
                        </View>
                        <Text style={styles.price}>Rp 420.000</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
      
    </ScreenContainer>
  );
};
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
    },
    headerStyle: {
        backgroundColor: '#74b0e8',
        paddingBottom: 15,
        elevation: 5,
      },
    searchBar: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10,
    },
    searchIcon: {
        backgroundColor: 'white',
        paddingVertical: 11,
        paddingHorizontal: 20,
        borderTopLeftRadius: 50,
        borderBottomLeftRadius: 50,
        color:'#c4c4c4'
    },
    search: {
        paddingVertical: 10,
        width: 310,
        backgroundColor: 'white',
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
    },
    banner: {
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 370, 
        height: 180, 
        borderRadius: 10,
    },
    menuRow: { 
        flexDirection: 'row',
    },
    menuImage: {
        width: 60, 
        height: 60, 
    },
    menuIcon: {
        margin: 10, 
        alignItems: 'center',
        justifyContent: 'center',
    },
    product: {
        backgroundColor: '#74b0e8', 
        padding: 10,
        marginTop: 15,
    },
    productText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    text: {
        fontSize: 18,
        color: 'white',
    },
    more: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textMore: {
        fontSize: 12,
        color: 'white',
    },
    cardSet: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    card: {
        backgroundColor: 'white', 
        width: 170,
        height: 280,
        borderRadius: 10,
        marginVertical: 15,
        marginHorizontal: 2,
        elevation: 5,
    },
    cardImage: {
        width: 170, 
        height: 170, 
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    cardText: {
        margin: 8, 
    },
    discText: { 
        flexDirection: 'row',
    },
    disc: {
        textDecorationLine: 'line-through', 
        textDecorationStyle: 'solid',
        marginHorizontal: 8, 
        color: '#c4c4c4',
    },
    price: {
        marginHorizontal: 8, 
        fontSize: 20,
        color: '#4793D7',
    },
  }); 