import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

import { Opening1, Opening2, Opening3 } from './OpeningScreen';
import { Login, Reg } from './LoginScreen';
import { Main } from './MainScreen';
import { About } from './AboutScreen';
import { Detail1, Detail2 } from './DetailScreen';
import { Product } from './ProductScreen';

const OpenStack = createStackNavigator();
const LoginStack = createStackNavigator();
const MainStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const OpenStackScreen = () => (
  <OpenStack.Navigator screenOptions={{ headerShown: false }} >
    <OpenStack.Screen name="Open1" component={Opening1} />
    <OpenStack.Screen name="Open2" component={Opening2} />
    <OpenStack.Screen name="Open3" component={Opening3} />
    {/* <OpenStack.Screen name="Login" component={LoginStackScreen} /> */}
  </OpenStack.Navigator>
)

const LoginStackScreen = () => (
  <LoginStack.Navigator screenOptions={{ headerShown: false }} >
    <LoginStack.Screen name="Login" component={Login} />
    <LoginStack.Screen name="Reg" component={Reg} />
  </LoginStack.Navigator>
)

const MainStackScreen = () => (
  <MainStack.Navigator screenOptions={{ headerShown: false }} >
    <MainStack.Screen name="Main" component={Main} />
    <MainStack.Screen name="Detail1" component={Detail1} />
    <MainStack.Screen name="Detail2" component={Detail2} />
  </MainStack.Navigator>
)

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Start" component={OpenStackScreen} />
      <Drawer.Screen name="Home" component={MainStackScreen} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="Log out" component={LoginStackScreen} />
    </Drawer.Navigator>
  </NavigationContainer>
);