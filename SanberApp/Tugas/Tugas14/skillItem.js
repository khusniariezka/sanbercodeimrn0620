import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import data from './skillData.json';

export default class App extends Component {
    render() {
        const newData = data.map((data) => {
            return(
                <View style={styles.card} key={data.items.id}>
                    <Text style={styles.name}>{data.items.skillName}</Text>
                    <Text style={styles.skill}>{data.items.percentageProgress}</Text>
                    <Text style={styles.skill}>{data.items.categoryName}</Text>
                </View>
            )
        })
    }   
} 