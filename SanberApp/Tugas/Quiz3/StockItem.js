import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class StockItem extends Component {
    render() {
        let stock = this.props.stock;
        return (
            <View style={styles.container}>
              <View style={styles.card}>
                <View style={styles.content}>
                    <Image source={{uri:stock.gambaruri}} style={{width:100, height:100, borderRadius:3}}/>
                    <Text numberOfLines={1}>{stock.nama}</Text>
                    <Text style={styles.price}>Rp {formatMoney(stock.harga)}</Text>
                    <Text>Sisa Stok: {stock.stock}</Text>
                </View>
                <Button title="BELI" />
              </View>
            </View>
        )
    }
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
  
      const negativeSign = amount < 0 ? "-" : "";
  
      let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
      let j = (i.length > 3) ? i.length % 3 : 0;
  
      return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
    } catch (e) {
    }
  };

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 5,
      justifyContent: 'center',
      backgroundColor: '#cfcfcf',
    },
    card: {
        padding: 15,
        backgroundColor: '#fff',
      },
    content: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    price: {
        fontSize: 18,
        color: 'blue',
    },
  });
