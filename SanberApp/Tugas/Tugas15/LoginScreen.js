import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Linking } from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topLogo}>
            <Image source={require('./images/logo1.png')} />
        </View>
        <View style={styles.formLogin}>
          <TextInput style={styles.inputSet} placeholder="Username / Email" />
          <TextInput style={styles.inputSet} placeholder="Password" />
          <TouchableOpacity>
            <View style={styles.btnLogin}>
                <Text style={styles.textButton}>L O G I N</Text>
            </View>
          </TouchableOpacity>
          <Text style={styles.note}>
              <Text>Don't have an account </Text>
              <Text style={styles.link} onPress={() => Linking.openURL('https://www.google.com')} >Sign Up</Text>
              <Text> here</Text>
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#f8f7f0',
      alignItems: 'center',
      justifyContent: 'center',
    },
    topLogo: {
        marginBottom: 40,
    },
    formLogin: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputSet: {
        width: 300, 
        height: 40, 
        borderColor: '#9d705f', 
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 3,
        borderWidth: 1,
        margin: 15,
        padding: 10,
    },
    btnLogin: {
        width: 300, 
        height: 42, 
        margin: 15,
        backgroundColor: '#9d705f',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 3,
    },
    textButton: {
        color: 'white',
    },
    note: {
        flexDirection: 'row',
    },
    link: {
        color: '#9d705f',
        fontWeight: 'bold',
    },
  });