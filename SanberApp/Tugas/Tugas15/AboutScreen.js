import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
          <StatusBar translucent={false} backgroundColor={'white'}/>
          <View style={styles.navBar}>
          <Image source={require('./images/logo2.png')} style={{width: 100, height: 35}} />
          <View>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="menu" size={25} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.body}>
            <View style={styles.card}>
                <Image source={require('./images/profile.png')} style={{width: 150, height: 150}} />
                <Text style={styles.name}>Riezka Layli Khusnia</Text>
                <Text style={styles.skill}>React Native Developer</Text>
            </View>
            <View style={styles.socMed}>
                <Text style={styles.title}>F O L L O W   M E</Text>
                <View style={styles.socmedIcon}>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/sm-fb.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/sm-twitter.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/sm-ig.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                </View>
            </View>
            <View style={styles.line}></View>
            <View style={styles.socMed}>
                <Text style={styles.title}>P O R T O F O L I O</Text>
                <View style={styles.socmedIcon}>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/dribbble.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/gitlab.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                    <TouchableOpacity><Image style={styles.shadowIcon} source={require('./images/github.png')} style={{width: 50, height: 50}} /></TouchableOpacity>
                </View>
            </View>
        </View>

        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="person" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="home" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="folder-special" size={25} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f7f0',
    fontFamily: 'Roboto',
  },
  navBar: {
    height: 60,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    width: 330, 
    height: 300, 
    margin: 15,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
  },
  name: {
    fontSize: 25,
    marginTop: 20,
    marginBottom: 5,
  },
  skill: {
    color: '#9d705f',
    fontWeight: 'bold',
  },
  socMed: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 30,
  },
  title: {
    marginBottom: 15,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#9d705f',
  },
  socmedIcon: {
    flexDirection: 'row',
  },
  shadowIcon: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.9,
    shadowRadius: 2,
    elevation: 3,
  },
  line: {
    width: 50,
    height: 1,
    backgroundColor: '#9d705f',
  },
  tabBar: {
    backgroundColor: 'white',
    height: 55,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});