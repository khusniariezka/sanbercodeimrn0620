import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Logo from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
          <StatusBar translucent={false} backgroundColor={'white'}/>
          <View style={styles.navBar}>
            <Image source={require('./images/logo2.png')} style={{width:100, height:35}} />
          <View>
            <TouchableOpacity>
              <Icon style={styles.navItem} name="menu" size={25} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.body}>
          <ScrollView>
          <View style={styles.profile}>
            <Image source={require('./images/profile.png')} style={{width:50, height:50}} />
            <Text style={styles.dataProfile}>Riezka Layli Khusnia</Text>
          </View>
          <View style={styles.mySkill}>
            <View style={styles.line}></View>
              <Text style={styles.skillTitle}>My Skill</Text>
            <View style={styles.line}></View>
          </View>
          <View style={styles.cardSkill}>
            <Text style={styles.title}>BAHASA PEMROGRAMAN</Text>
            <ScrollView horizontal>
              <View style={styles.card}>
                <Logo name="language-javascript" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>JavaScript</Text>
                  <Text>Beginner</Text>
                  <Text style={styles.percent}>30%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
              <View style={styles.card}>
                <Logo name="language-python" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>Python</Text>
                  <Text>Intermediate</Text>
                  <Text style={styles.percent}>70%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
            </ScrollView>
            <Text style={styles.title}>FRAMEWORK / LIBRARY</Text>
            <ScrollView horizontal>
              <View style={styles.card}>
                <Logo name="react" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>React Native</Text>
                  <Text>Intermediate</Text>
                  <Text style={styles.percent}>50%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
              <View style={styles.card}>
                <Logo name="laravel" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>Laravel</Text>
                  <Text>Advance</Text>
                  <Text style={styles.percent}>100%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
            </ScrollView>
            <Text style={styles.title}>TECHNOLOGY</Text>
            <ScrollView horizontal>
              <View style={styles.card}>
                <Logo name="git" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>Git</Text>
                  <Text>Intermediate</Text>
                  <Text style={styles.percent}>75%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
              <View style={styles.card}>
                <Logo name="gitlab" size={100} /> 
                <View style={styles.cardContent}>
                  <Text style={styles.skill}>Gitlab</Text>
                  <Text>Intermediate</Text>
                  <Text style={styles.percent}>60%</Text>
                </View>
                <Logo name="chevron-right" size={50} />
              </View>
            </ScrollView>
          </View>
          </ScrollView>
        </View>

        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="person" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="home" size={25} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name="folder-special" size={25} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f7f0',
    fontFamily: 'Roboto',
  },
  navBar: {
    height: 60,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  body: {
    flex: 1,
  },
  profile: {
    marginTop: 20,
    marginHorizontal: 15,
    padding: 10,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#9d705f',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
  },
  dataProfile: {
    marginLeft: 15,
    fontSize: 25,
    color: '#fff',
  },
  mySkill: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  skillTitle: {
    fontSize: 30,
    color: '#9d705f',
  },
  line: {
    width: 100,
    height: 1,
    backgroundColor: '#9d705f',
  },
  title: {
    marginLeft: 15,
    marginTop: 10,
    color: '#9d705f',
    fontWeight: 'bold',
  },
  card: {
    width: 330, 
    height: 150, 
    margin: 15,
    padding: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    flexDirection: 'row',
  },
  cardContent: {
    alignItems: 'center',
  },
  skill: {
    fontSize: 25,
  },
  percent: {
    fontSize: 40,
  },
  tabBar: {
    backgroundColor: 'white',
    height: 55,
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});