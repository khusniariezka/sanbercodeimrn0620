import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
          <StatusBar translucent={false} backgroundColor={'white'}/>
          <Text>Halaman Tambah</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f7f0',
    fontFamily: 'Roboto',
    alignItems: 'center',
    justifyContent: 'center',
  },
});