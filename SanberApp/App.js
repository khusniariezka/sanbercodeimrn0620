import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Main from './Tugas/Tugas14/components/Main';
import Login from './Tugas/Tugas15/LoginScreen';
import About from './Tugas/Tugas15/AboutScreen';
import Skill from './Tugas/Tugas15/SkillScreen';
import Project from './Tugas/Tugas15/ProjectScreen';
import Add from './Tugas/Tugas15/AddScreen';
import Nav from './Tugas/Tugas15/index';
import MyApp from './Latihan/MyApp/index';
import Quiz from './Tugas/Quiz3/index';
import Login2 from './Tugas/Quiz3/LoginScreen';

export default function App() {
  return (
    <Quiz />
  );
}