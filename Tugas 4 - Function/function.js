//Soal 1
console.log("---SOAL 1---")
function teriak() {
	return "Halo Sanbers!"
}
 
console.log(teriak())

//Soal 2	
console.log("---SOAL 2---")
function kalikan(num1, num2) {
	return num1 * num2;
}

var hasilKali = kalikan(12, 4);
console.log(hasilKali)

//Soal 3
console.log("---SOAL 3---")
function introduce(name, age, address, hobby) {
	var a = "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+" dan saya punya hobby yaitu "+hobby+"!"
	return a;
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)