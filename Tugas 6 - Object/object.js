// Soal 1
console.log("\n---SOAL 1---")
function arrayToObject(arr) {
    var d = {};
    var now = new Date()
    var thisYear = now.getFullYear()
    for (var i = 0; i < arr.length; i++) {
        var ageNow = thisYear - arr[i][3];
        if (ageNow > 0 && ageNow < 100) {
            d = {firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: ageNow};
           
        } else {
            d = {firstName: arr[i][0], lastName: arr[i][1], gender: arr[i][2], age: "Invalid Birth Year"};
        }
        console.log(i+1 + ". " + arr[i][0] + " " + arr[i][1] + ": ", d)
    }
    return "";
}
people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
console.log(arrayToObject(people))
console.log(arrayToObject(people2))
console.log(arrayToObject([]))

// Soal 2
console.log("---SOAL 2---")
function shoppingTime(memberId, money) {
    var a = (memberId, money)
    var memberID = memberId
    var sisa = money
    var barang = []
    var counter = 0;

    if (!memberID || a === undefined) {
        return ("Mohon maaf, toko X hanya berlaku untuk member saja")
    } else if (money < 50000) {
        return ("Mohon maaf, uang tidak cukup")
    } else {

        var d = {}
        d.memberId = memberId
        d.money = money

        if (sisa >= 1500000) {
            sisa = sisa - 1500000
            barang.push('Sepatu Stacattu')
        } if (sisa >= 500000) {
            barang.push('Baju Zoro')
            sisa = sisa - 500000
        } if (sisa >= 250000) {
            barang.push('Baju H&N')
            sisa = sisa - 250000
        } if (sisa >= 175000) {
            barang.push('Sweater Uniklooh')
            sisa = sisa - 175000
        } if (sisa >= 50000) {
            barang.push('Casing Handphone')
            sisa = sisa - 50000
        }    
    }

    if (sisa < 0) {
        sisa = 0
    } else {
        sisa = sisa
    }
    d.listPurchased = barang
    d.changeMoney = sisa
    return d;
}
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000));
  console.log(shoppingTime('234JdhweRxa53', 15000));
  console.log(shoppingTime());

// Soal 3
console.log("\n---SOAL 3---")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var d = {};
    
    for (var i = 0; i < arrPenumpang.length; i++) {
        var asal = arrPenumpang[i][1];
        var tiba = arrPenumpang[i][2];
        var jarak = 0;
        for (var k=0; k<rute.length; k++) {
            if (rute[k] === asal) {
                for (var l=k+1; l<rute.length; l++) {
                    jarak += 1;
                    if (rute[l] === tiba) {
                        var totalBayar = jarak * 2000;
                    }
                }
            }
        }
        d = {penumpang: arrPenumpang[i][0], naikDari: asal, tujuan: tiba, bayar: totalBayar};
        console.log(d)
    }
    return "";
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([])); 