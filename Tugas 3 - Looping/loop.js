//Soal 1
console.log('---SOAL 1---');
var angka = 1;
console.log('LOOPING PERTAMA');
while (angka <= 20) {
	if (angka % 2 == 0) {
		console.log(angka + ' - I love coding');
	}
	angka++;
}

console.log('LOOPING KEDUA');
while (angka >= 1) {
	if (angka % 2 == 0) {
		console.log(angka + ' - I will become a mobile developer');
	}
	angka--;
}

//Soal 2
console.log('---SOAL 2---');
for (var i = 1; i <= 20; i++) {
	if ((i % 3) == 0 && (i % 2) == 1) {
		console.log(i + ' - I Love Coding');
	} else if ((i % 2) == 1) {
		console.log(i + ' - Santai');
	} else if ((i % 2) == 0) {
		console.log(i + ' - Berkualitas');
	} 
}

//Soal 3
console.log('---SOAL 3---');
for (var i = 1; i <= 4; i++){
	for (var j = 1; j <= 8; j++){
		process.stdout.write('#');
	}
	console.log();
}

//Soal 4
console.log('---SOAL 4---');
for (var i = 1; i <= 7; i++){
	for (var j = 1; j <= i; j++){
		process.stdout.write('#');
	}
	console.log();
}

//Soal 5
console.log('---SOAL 5---');
for (var i = 1; i <= 8; i++){
	for (var j = 1; j <= 4; j++){
		if ((i % 2) == 1) {
			process.stdout.write(' ');
			process.stdout.write('#');
		} else if ((i % 2) == 0) {
			process.stdout.write('#');
			process.stdout.write(' ');
		}
	}
	console.log();
}